<?php

use App\Enums\MembershipType;

return [
    
    MembershipType::class => [
        MembershipType::SILVER => 'Silver',
        MembershipType::GOLD => 'Gold',
        MembershipType::PLATINUM => 'Platinum';
        MembershipType::BLACK => 'Black',
        MembershipType::VIP => 'VIP',
        MembershipType::VVIP => 'VVIP',
    ]
]