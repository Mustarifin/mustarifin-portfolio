@extends('layouts.app')

@section('content')
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header step-text">{{ __('Step 1') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('post.register.form') }}" class="registration-form">
                        @csrf

                        <div class="step-1">
                            <div class="form-group row">
                                <label for="first_name" class="col-md-4 col-form-label text-md-right">{{ __('First Name') }}</label>

                                <div class="col-md-6">
                                    <input id="first_name" type="text" class="form-control @error('first_name') is-invalid @enderror" name="first_name" value="{{ old('first_name') }}" autocomplete="first_name">

                                        <span class="text-danger error-first_name d-none"  role="alert">
                                            <strong>error</strong>
                                        </span>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="last_name" class="col-md-4 col-form-label text-md-right">{{ __('Last Name') }}</label>

                                <div class="col-md-6">
                                    <input id="last_name" type="text" class="form-control @error('last_name') is-invalid @enderror" name="last_name" value="{{ old('last_name') }}"  autocomplete="last_name" autofocus>

                                        <span class="text-danger error-last_name d-none"  role="alert">
                                            <strong>error</strong>
                                        </span>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}">

                                        <span class="text-danger error-email d-none"  role="alert">
                                            <strong>error</strong>
                                        </span>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="term_and_condition" class="col-md-4 col-form-label text-md-right">{{ __('Term and Condition') }}</label>

                                <div class="col-md-6">
                                    {{ Form::checkbox('term_and_condition', null) }}
                                        <br>
                                        <span class="text-danger error-term_and_condition d-none"  role="alert">
                                            <strong>error</strong>
                                        </span>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password"  autocomplete="new-password">

                                        <span class="text-danger error-password d-none"  role="alert">
                                            <strong>error</strong>
                                        </span>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                                <div class="col-md-6">
                                    <input id="password-confirm" type="password" class="form-control error-term_and_condition" name="password_confirmation"  autocomplete="new-password">
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary btn-step-1">
                                        {{ __('Next') }}
                                    </button>
                                </div>
                            </div>

                        </div>

                        <div class="step-2" style="display:none">
                            <div class="form-group row">
                                <label for="address" class="col-md-4 col-form-label text-md-right">{{ __('Address') }}</label>

                                <div class="col-md-6 listAllamat">
                                        <textarea name="address[]" cols="10" class="form-control input-alamat" rows="4"></textarea>

                                        <span class="text-danger error-address d-none"  role="alert">
                                            <strong>error</strong>
                                        </span>
                                        <br>
                                        <button type="button" class="btn btn-success add-alamat">Tambah</button>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="date_of_birth" class="col-md-4 col-form-label text-md-right">{{ __('Date Of Birth') }}</label>

                                <div class="col-md-6">
                                    <input id="date_of_birth" type="text" class="form-control @error('date_of_birth') is-invalid @enderror custom-datepicker" name="date_of_birth" value="{{ old('date_of_birth') }}" autocomplete="off">

                                    <span class="text-danger error-date_of_birth d-none"  role="alert">
                                            <strong>error</strong>
                                        </span>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="membership_type" class="col-md-4 col-form-label text-md-right">{{ __('Membership Type') }}</label>

                                <div class="col-md-6">
                                    {{ Form::select('membership_type', App\Enums\MembershipType::getKeys(), null, ['class' => 'select_member']) }}
                                    <span class="text-danger error-membership_type d-none"  role="alert">
                                            <strong>error</strong>
                                        </span>
                                </div>
                            </div>
                            <br>
                            <h3>Credit Card</h3>
                            <div class="form-group row">
                                <label for="number" class="col-md-4 col-form-label text-md-right">{{ __('Credit Card Number') }}</label>

                                <div class="col-md-6">
                                    <input id="number" type="text" class="form-control @error('number') is-invalid @enderror" name="number" value="{{ old('number') }}" >

                                    <span class="text-danger error-number d-none"  role="alert">
                                            <strong>error</strong>
                                        </span>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="number" class="col-md-4 col-form-label text-md-right">{{ __('Credit Card Type') }}</label>

                                <div class="col-md-6">
                                    <input id="type" type="text" class="form-control @error('type') is-invalid @enderror" name="type" value="{{ old('type') }}" >

                                    <span class="text-danger error-type d-none"  role="alert">
                                            <strong>error</strong>
                                        </span>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="number" class="col-md-4 col-form-label text-md-right">{{ __('Credit Card Exp. Date') }}</label>
                                
                                <div class="col-md-6">
                                    <input id="exp_date" type="text" class="form-control @error('exp_date') is-invalid @enderror custom-datepicker" name="exp_date" value="{{ old('exp_date') }}" autocomplete="off">

                                    <span class="text-danger error-exp_date d-none"  role="alert">
                                            <strong>error</strong>
                                        </span>
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Register') }}
                                    </button>
                                </div>
                            </div>

                        </div>

                        <div class="step-3" style="display:none">
                        <div class="alert alert-info" role="alert">
                            Success registrasi
                        </div>
                        <div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function(){
    $('.custom-datepicker').datepicker();
});
</script>
@endsection
