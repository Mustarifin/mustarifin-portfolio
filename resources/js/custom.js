const { default: Axios } = require("axios");

var custom = {
    init : function(){
        custom.next1();
        custom.submitFormRegistration();
        custom.select2();
        custom.datePicker();
        custom.addAddress();
    },

    next1: function(){
        var btn = $('.btn-step-1');
        
        if(!btn.length) return;
        
        btn.click(function(e){
            e.preventDefault();
            $('.step-1').hide();
            $('.step-2').show();
            $('.card-header').text('Step 2');
        });
    },

    submitFormRegistration: function(){
        var form = $('.registration-form');

        if(!form.length) return;

        form.submit(function(e){
            e.preventDefault();
            var data = form.serialize(), 
            url = form.attr('action');

            axios.post(url, data).then((res) => {
                $('.step-3').show();
                $('.step-2').hide();
                $('.step-1').hide();
            }).catch((err) => {
                if (err.response.status === 422) {
                    $.each( err.response.data.errors, function( key, value ) {
                        $('.error-'+key).removeClass('d-none').addClass('d-block').find('strong').html(value[0]);
                        if($('.error-'+key).closest('.row').parent().prop('className') == 'step-1' || $('.step-1').is(":visible")){
                            var parrent = $('.error-'+key).closest('.row').parent().prop('className');
                            $('.'+parrent).show();
                            $('.step-2').hide();
                        } else {
                            $('.step-2').show();
                        }
                    });
                }
            });
        });
    },

    select2: function(){
        var select2 = $('.select_member');

        if(!select2.length) return;

        $('.select_member').select2();
        $('.select2-container').width("100%"); 
    },

    datePicker: function(){
        var datepicker = $('.custom-datepicker');

        if(!datepicker.length) return;

        $('.custom-datepicker').datepicker();
    },

    addAddress: function(){
        var addAddress = $('.add-alamat');

        if(!addAddress.length) return;

        addAddress.click(function(){
            var newAddress = $('.input-alamat:first').clone();

            $(newAddress).insertAfter('.input-alamat:first');
        });
    }
}

$(document).ready(function () {
    custom.init();
});