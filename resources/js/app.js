/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('./i18n_datepicker');
require('./custom');
require('webpack-jquery-ui/css');
require('webpack-jquery-ui');
require('select2');
