<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('registration', 'CustomRegistrationController@showFormRegistration')->name('show.register.form');
Route::post('registration', 'CustomRegistrationController@postRegistration')->name('post.register.form');
Auth::routes(['register' => false]);

Route::get('/home', 'HomeController@index')->name('home');
