<?php

namespace App\Enums;

use BenSampo\Enum\Enum;
use BenSampo\Enum\Contracts\LocalizedEnum;

/**
 * @method static static OptionOne()
 * @method static static OptionTwo()
 * @method static static OptionThree()
 */
final class MembershipType extends Enum implements LocalizedEnum
{
    const SILVER =   1;
    const GOLD =   2;
    const PLATINUM = 3;
    const BLACK = 4;
    const VIP = 5;
    const VVIP = 6;
}