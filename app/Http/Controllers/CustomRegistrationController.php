<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CustomRegistration;
use App\Models\User;
use App\Models\Address;
use App\Models\CreditCard;

class CustomRegistrationController extends Controller
{
    public function showFormRegistration()
    {
        return view('auth.register');
    }
    public function postRegistration(CustomRegistration $request, User $user)
    {
        $user = $user->create($request->getDataUser());
        foreach ($request->address as $key => $value) {
            Address::create([
                'value' => $value,
                'user_id' => $user->id,
            ]);
        }

        $dataCreditCard = $request->getDataCard();
        $dataCreditCard['user_id'] = $user->id;
        $creditCard = CreditCard::create($dataCreditCard);

        return response()->json(['message' => __('Registration Success'), 'success' => true]);
    }
}
