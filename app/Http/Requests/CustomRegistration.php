<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use LVR\CreditCard\CardCvc;
use LVR\CreditCard\CardNumber;
use LVR\CreditCard\CardExpirationYear;
use LVR\CreditCard\CardExpirationMonth;

class CustomRegistration extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $expiration_year = (int)date('Y', strtotime($this->exp_date));
        $expiration_month = date('mm', strtotime($this->exp_date));
        // dd($expiration_year);
        return [
            'first_name' => 'nullable|max:255|',
            'last_name' => 'required|max:255',
            'email' => 'required|regex:/^\S*$/u|unique:users,email',
            'term_and_condition' => 'required',
            'date_of_birth' => 'required|date',
            'membership_type' => 'required',
            'password' => 'required|confirmed',
            'address.*' => 'required',
            'number' => ['required', new CardNumber],
            'expiration_year' => [new CardExpirationYear($expiration_year)],
            'expiration_month' => [new CardExpirationMonth($expiration_month)],
            'cvc' => [new CardCvc($this->number)],
            'type' => 'required|max:255',
            'exp_date' => 'required|date',
        ];
    }

    public function getDataUser()
    {
        return [
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'email' => $this->email,
            'term_and_condition' => $this->term_and_condition ? true : false,
            'date_of_birth' => $this->date_of_birth,
            'membership_type' => $this->membership_type,
            'password' => bcrypt($this->password),
        ];
    }

    public function getDataCard()
    {
        return [
            'number' => $this->number,
            'type' => $this->type,
            'exp_date' => $this->exp_date,
            'user_id' => '',
        ];
    }
}
